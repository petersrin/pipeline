﻿using UnityEngine;
using UnityEngine.Rendering;

namespace TempSpace
{
    [CreateAssetMenu(menuName = "Rendering/Custom Render Pipeline")]
    public class CustomRenderPipelineAsset : RenderPipelineAsset
    {
        protected override RenderPipeline CreatePipeline() => new CustomRenderPipeline();
    }
}
