using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

namespace TempSpace
{
    public partial class CameraRenderer
    {
    #if UNITY_EDITOR
        
        private string SampleName { get; set; }
        
        private static ShaderTagId[] legacyShaderTagIds =
        {
            new ShaderTagId("Always"), new ShaderTagId("ForwardBase"), new ShaderTagId("PrepassBase"),
            new ShaderTagId("Vertex"), new ShaderTagId("VertexLMRGBM"), new ShaderTagId("VertexLM")
        };

        private static Material errorMaterial;

        partial void DrawUnsupportedShaders()
        {
            if (errorMaterial == null) errorMaterial = new Material(Shader.Find("Hidden/InternalErrorShader"));
            var drawSettings = new DrawingSettings(legacyShaderTagIds[0], new SortingSettings(_camera))
            {
                overrideMaterial = errorMaterial
            };

            for (var i = 1; i < legacyShaderTagIds.Length; i++)
                drawSettings.SetShaderPassName(i, legacyShaderTagIds[i]);

            var filteringSettings = FilteringSettings.defaultValue;
            _context.DrawRenderers(_cullingResults, ref drawSettings, ref filteringSettings);
        }

        partial void DrawGizmos()
        {
            if (!Handles.ShouldRenderGizmos()) return;
            
            _context.DrawGizmos(_camera, GizmoSubset.PreImageEffects);
            _context.DrawGizmos(_camera, GizmoSubset.PostImageEffects);
        }

        partial void PrepareForSceneWindow()
        {
            if (_camera.cameraType == CameraType.SceneView)
            {
                ScriptableRenderContext.EmitWorldGeometryForSceneView(_camera);
            }
        }

        private void PrepareBuffer()
        {
            Profiler.BeginSample("Editor Only");
            _buffer.name = SampleName = _camera.name;
            Profiler.EndSample();
        }
#else
const string SampleName = bufferName;
#endif
    }
}